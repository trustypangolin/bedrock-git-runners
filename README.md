# Project README

## Project Documentation

This README provides a brief overview of the project's relevant *documentation* structure and instructions on how to get it up and running.

```
.
│   catalog-info.yaml       <-- Backstage Catalog
│   docker-compose-docs.yml <-- Various Docker Services for local Doc development
│   LICENSE                 <-- OpenSource Licensing
│   README.md               <-- You are here
│
└───docs
    │   mkdocs.yml          <-- Project Documentation served via Docker or Backstage
    └───docs
        │   index.md
        └───gitlab          <-- GitLab Runner Documentation
```

Project Documentation assumes a working [Backstage and Traefik](https://gitlab.com/trustypangolin/backstage/blob/main/docker-compose.yml "Backstage Traefik") setup and is integrated with Backstage catalog system. However it is possible to load a basic docs website for dev purposes

```
docker compose -f docker-compose-docs.yml up gitrunner-docs -d
```

## Project Structure

The project's files (minus documentation) is organized as follows:

```
C:.
│   .env.template                # .env template for docker-compose.yml
│   .gitignore                   
│   .gitlab-ci.yml               # Gitlab Pipeline to build docker image
│   docker-compose.yml
│   Dockerfile-gitlab            # Gitlab runner Compose
└───src                          # Extra tooling for runners
        install_aws.sh
        install_docker.sh
        install_hashicorp.sh
        install_systemd.sh
        install_tools.sh
```

The main project files and directories are as follows:
- `.env.template`: Environment configuration files.
- `.gitignore`: Git version control configuration.
- `.gitlab-ci.yml`: GitLab CI/CD pipeline configuration.
- `docker-compose.yml`: Docker Compose configuration.
- `Dockerfile-gitlab`: Docker image configuration for GitLab Runners.

In addition to these files, there are source subdirectorie:
- `src`: Contains various shell scripts for installing AWS, Docker, HashiCorp tools, systemd, and additional system tools.

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository to your local machine:

   ```bash
   git clone git@gitlab.com:trustypangolin/bedrock-git-runners.git
   ```

2. Set up your environment by creating or modifying the `.env` file based on `.env.template` file with the necessary configuration parameters.

3. Install Docker on your system if it's not already installed.

4. Configure GitLab CI/CD by modifying the `.gitlab-ci.yml` file to suit your project's needs. See project docs for more information

5. Start the project by running Docker Compose using the provided configuration:

   ```bash
   docker-compose up -d
   ```

Your project should now be up and running, with Docker containers and CI/CD pipelines configured as per the `.gitlab-ci.yml` file.

Please refer to specific documentation for more detailed information and customization options.