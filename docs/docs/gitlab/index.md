# Docker Compose

This documentation provides an overview and explanation of the Docker Compose file:

### Service: dind

```yaml
  dind:
    image: docker:24-dind
    restart: always
    privileged: true
    environment:
      - DOCKER_TLS_CERTDIR=
      - DOCKER_HOST=tcp://0.0.0.0:2375
    command:
      - --storage-driver=overlay2
      - --tls=false
```

- **Image:** This service uses the "docker:24-dind" Docker image, which provides a Docker-in-Docker (DinD) environment.
- **Restart:** The service is configured to always restart automatically if it stops.
- **Privileged:** Setting "privileged" to true allows the service to run with additional privileges.
- **Environment:** The "DOCKER_TLS_CERTDIR" environment variable is set to an empty string, to disable TLS certificate for Docker.
- **Command:** The Docker daemon is launched with the "--storage-driver=overlay2" and "--tls=false" options

### Service: gitlab-register

```yaml
  gitlab-register:
    image: gitlab/gitlab-runner:latest
    environment:
      - CI_SERVER_URL=${CI_SERVER_URL}            # Normally https://gitlab.com, change for Self-Hosted in .env
      - REGISTER_NON_INTERACTIVE=true             # Register without prompts
      - REGISTRATION_TOKEN=${REGISTRATION_TOKEN}  # This is the old way of registration, but here for backward compatibility
      - TOKEN=${TOKEN}                            # New way of registering Gitlab runners
    command: |
      register --token ${TOKEN} --executor docker --docker-image=docker:24-dind --docker-volumes=/var/run/docker.sock:/var/run/docker.sock
    volumes:
      - gitlab-runner:/etc/gitlab-runner
```

A single run service that will create the config.toml file in the volume shared with the actual gitlab runner. 

- **Image:** This service uses the `gitlab/gitlab-runner:latest` Docker image, which is used to register a GitLab Runner.
- **Environment:** Several environment variables are set, including 
  * **CI_SERVER_URL** the GitLab server URL
  * **REGISTRATION_TOKEN** the legacy registration token
  * **TOKEN** the registration token
  * **REGISTER_NON_INTERACTIVE** is set to true to run the registration process non-interactively.
- **Command:** The service runs the "register" command to register the GitLab Runner with specific configurations, such as using the "docker" executor and the "docker:24-dind" image. It also specifies the Docker socket that will be available to the runner.
- **Volumes:** The service mounts the "gitlab-runner" volume into the container at "/etc/gitlab-runner"

### Service: gitlab-runner

```yaml
  gitlab-runner:
    restart: always
    image: papina/gitlab-runner:latest
    environment:
      - DOCKER_HOST=tcp://dind:2375
    volumes:
      - gitlab-runner:/etc/gitlab-runner
```

- **Restart:** Like the "dind" service, this service is configured to always restart automatically if it stops.
- **Image:** This service uses the "papina/gitlab-runner:latest" Docker image, which is a customized GitLab Runner image with additional tools such as aws cli and ssm, terraform, and additional shell tools with the configurations.
- **Environment:** The "DOCKER_HOST" environment variable is set to "tcp://dind:2375," indicating that this GitLab Runner should communicate with the Docker daemon in the "dind" service.
- **Volumes:** The service mounts the "./gitlab-runner" directory into the container at "/etc/gitlab-runner."

```yaml
volumes:
  gitlab-runner:
```

- **gitlab-runner:** Shared volume, will have /etc/gitlab-runner/config.toml configured

This Docker Compose file is designed to create a Gitlab runner environment for GitLab CI/CD integration, including a Docker-in-Docker (DinD) service, a GitLab Runner registration service, and a customized GitLab Runner with specific Docker configurations and tools installed. The services are orchestrated to work together for automated CI/CD tasks, and allow for Gitlab Pipelines to be run locally without using Gitlab compute usage

### Compose Services

```plantuml
@startuml

!define SPRITESURL https://raw.githubusercontent.com/rabelenda/plantuml-sprites/v14.0/sprites

autonumber
hide footbox
skinparam SequenceMessageAlign center
skinparam ParticipantPadding 20
skinparam BoxPadding 20

box "Run Once"
  participant "Gitlab Register" as register
end box

box "Running" #LightBlue
  database "Volume" as volume
  participant "GitLab Runner" as runner
  participant "Docker-in-Docker (DinD)" as dind
end box

participant "Gitlab Server" as git


register -> volume : Write config.toml
runner <- volume : Read config.toml
runner <-> dind : Docker Operations
git <-> runner : Pipeline (.gitlab-ci)
@enduml
```
