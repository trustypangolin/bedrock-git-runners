# Gitlab Server Setup

This documentation provides an overview of Gitlab Cloud User, Group and Project settings required for a CI/CD runner workflow

## Gitlab Account
https://gitlab.com/-/profile/personal_access_tokens

![Add Personal Access Token](images/add-token.png){: style="width:250px"}

## Gitlab Group
https://gitlab.com/groups/trustypangolin/-/settings/ci_cd

![Disable Gitlab Cloud Runners](images/shared-runners.png){: style="width:250px"}

https://gitlab.com/groups/trustypangolin/-/runners

![New Group Runners](images/new-group-runner.png){: style="width:250px"}

![Group Variables](images/group-variables.png){: style="width:250px"}



## Gitlab Project
https://gitlab.com/trustypangolin/bedrock-git-runners/-/settings/ci_cd

![View Active Runners](images/group-runners.png){: style="width:250px"}

![Add Repo Variables](images/add-repovar.png){: style="width:250px"}
